# auth-server

## Version 1
Login with client clidentials only, storage in memory.
curl -X POST -H "Accept: application/json" client:secret@localhost:8081/oauth/token -d "grant_type=client_credentials"

## Version 2
Login with password only, storage in memory.
curl -X POST -H "Accept: application/json" client:secret@localhost:8082/oauth/token -d grant_type=password -d username=admin -d password=admin

## Version 3
Login with password only, storage in database h2.
curl -X POST -H "Accept: application/json" client:secret@localhost:8083/oauth/token -d grant_type=password -d username=admin -d password=admin
curl http://localhost:8083/hello?access_token=c24755d2-8946-48e6-b6e9-f2af87277ce9
