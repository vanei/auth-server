package br.com.javanei.auth.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private UserDetailsManager userDetailsManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        System.out.println("@@@@@ DataInitializer.onApplicationEvent - 1 - (" + event + ")");
        System.out.println("@@@@@ DataInitializer.onApplicationEvent - 2 - userDetailsManager=" + userDetailsManager);

        User user = createUser("admin", "admin", "READ");
        System.out.println("@@@@@ DataInitializer.onApplicationEvent - 3 - user=" + user);
        userDetailsManager.createUser(user);
    }

    private User createUser(String username, String password, String authority) {
        return new User(username,
                passwordEncoder.encode(password),
                Arrays.asList(new SimpleGrantedAuthority(authority)));
    }
}
