package br.com.javanei.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationVersao3 {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationVersao3.class, args);
    }
}
