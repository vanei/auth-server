package br.com.javanei.auth.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Arrays;

@Component
public class DataInitializer implements ApplicationListener<ContextRefreshedEvent> {
    private static Logger LOG = LoggerFactory.getLogger(DataInitializer.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private UserDetailsManager userDetailsManager;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOG.info("@@@@@ DataInitializer.onApplicationEvent - 1 - (" + event + ")");
        LOG.info("@@@@@ DataInitializer.onApplicationEvent - 2 - userDetailsManager=" + userDetailsManager);

        if (!userDetailsManager.userExists("admin")) {
            User user = createUser("admin", "admin", "WRITE");
            LOG.info("@@@@@ DataInitializer.onApplicationEvent - 3 - user=" + user);
            userDetailsManager.createUser(user);
        }
    }

    private User createUser(String username, String password, String authority) {
        return new User(username,
                passwordEncoder.encode(password),
                Arrays.asList(new SimpleGrantedAuthority(authority)));
    }
}
